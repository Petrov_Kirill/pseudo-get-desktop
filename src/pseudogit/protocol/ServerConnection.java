package pseudogit.protocol;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kirill
 */
import pseudogit.entity.TextString;
import pseudogit.protocol.entity.Request;
import pseudogit.protocol.entity.Response;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ServerConnection implements ProtocolConnection {
    private static ServerConnection instance = null;

    private Socket socket;

    private InputStream sin;
    private OutputStream sout;
    private ObjectInputStream objin;
    private ObjectOutputStream objout;

    private ServerConnection(Socket socket) {
        setSocket(socket);
    }

    public static ServerConnection getInstance() throws IOException {
        if (instance == null){
            try {
                InetAddress serverAddress = InetAddress.getByName("192.168.43.19");
                Socket socket = new Socket(serverAddress, 12345);
                instance = new ServerConnection(socket);
            } catch (IOException e) {
                throw e;
            }
        }
        return instance;
    };

    public static void disconnect(){
        instance = null;
    }

    @Override
    public void setSocket(Socket socket) {
        try {
            this.socket = socket;
            sout = socket.getOutputStream();
            sin = socket.getInputStream();

            objout = new ObjectOutputStream(sout);
            objout.flush();
            objin = new ObjectInputStream(sin){
                @Override
                protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
                    ObjectStreamClass descriptor = super.readClassDescriptor();
                    if (descriptor.getName().endsWith(".Response")) {
                        return ObjectStreamClass.lookup(Response.class);
                    }
                    if (descriptor.getName().endsWith(".TextString")) {
                        return ObjectStreamClass.lookup(TextString.class);
                    }
                    return descriptor;
                }
            };
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isReady() {
        return (this.socket != null) && socket.isConnected();
    }

    @Override
    public Response sendRequest(Request request) {
        if (isReady()){
            try {
                objout.writeObject(request);
                objout.flush();
                objout.reset();

                Response response = (Response) objin.readObject();
                return response;

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new Response(404);
    }
}
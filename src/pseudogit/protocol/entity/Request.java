/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pseudogit.protocol.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Request implements Serializable {
    final private String type;
    final private List<Object> data;

    public Request() {
        this.type = "default";
        this.data = null;
    }

    public Request(String type, Object... data) {
        this.type = type;
        this.data = new ArrayList<>();
        this.data.addAll(Arrays.asList(data));
    }

    public String getType() {
        return type;
    }

    public List<Object> getData() {
        return data;
    }
}

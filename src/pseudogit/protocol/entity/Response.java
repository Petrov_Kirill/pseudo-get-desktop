/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pseudogit.protocol.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Response implements Serializable {
    final private int code;
    private List<Object> data;

    public Response() {
        this.code = -1;
        this.data = null;
    }

    public Response(int code, Object... data) {
        this.code = code;
        if (data != null) {
            this.data = new ArrayList<>();
            this.data.addAll(Arrays.asList(data));
        }
    }

    public int getCode() {
        return code;
    }

    public List<Object> getData() {
        return data;
    }
}

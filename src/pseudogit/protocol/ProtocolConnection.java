/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pseudogit.protocol;

import pseudogit.protocol.entity.Request;
import pseudogit.protocol.entity.Response;

import java.net.Socket;

/**
 *
 * @author kirill
 */
public interface ProtocolConnection {
    void setSocket(Socket socket);
    boolean isReady();
    Response sendRequest(Request request);
}

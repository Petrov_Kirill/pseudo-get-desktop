/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pseudogit.entity;

import java.util.List;

public class Commit {
    private int idCommit;
    private String date;
    private List<TextString> lines;

    public Commit(int idCommit, String date, List<TextString> lines) {
        this.idCommit = idCommit;
        this.date = date;
        this.lines = lines;
    }

    public int getIdCommit() {
        return idCommit;
    }

    public String getDate() {
        return date;
    }

    public List<TextString> getLines() {
        return lines;
    }
}
